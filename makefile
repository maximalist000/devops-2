all: build

init:
	mkdir myapp
	mkdir myapp/DEBIAN
	cp control myapp/DEBIAN/
	mkdir myapp/usr
	mkdir myapp/usr/bin

install: builddeb
	sudo apt install ./myapp.deb

uninstall:
	sudo apt remove myapp

builddeb: build
	dpkg-deb --build ./myapp

build:
	g++ main.cpp -o myapp/usr/bin/myapp